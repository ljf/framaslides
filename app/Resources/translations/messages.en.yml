app.name: <span class="frama">Frama</span><span class="services">slides</span>
app.presentation: "appName is an online slide editor service"

menu:
  newpresentation: New presentation
  mypresentations: My presentations
  mypresentations.desc: View my presentations
  templates: Templates
  pictures: Pictures
  pictures.desc: View my pictures
  disconnect: Disconnect
  login: Login
  register: Register
  groups.desc: Groups list
  admin: Admin
  config: Configuration
  search:
    placeholder: Search

presentations:
  title:
    edit: Edit the title (not working)
  createdAt: Date of creation
  updatedAt: Date of last update
  empty: You don't have any presentations yet, do you want to create one ?
  create: Create a new presentation
  explore: See public templates
  isTemplate: Template
  isPublic: Public
  share:
    title: Publish the presentation
    title.details: Publish the presentation generates a public link allowing everyone with it to view and fork your presentation
    input: Public URL for the presentation
    delete: Delete the public link
    delete.details: If you delete the public link for this presentation, it will no longer be possible to access it through this link.
  nbVersions:
    label: 'one version|%count% versions'
    desc: Number of versions
  nbSlides:
    label: 'only one slide|%count% slides'
    desc: Number of slides
  nbPictures:
    label: "{0}no pictures|{1}only one picture|]1,Inf[%count% pictures"
    desc: Number of pictures
  edit:
    desc: Edit the presentation
    back: Back to the presentations list
  delete:
    desc: Delete the presentation
  bookmark:
    desc: Manage the presentation
  download:
    title: Download the presentation
    desc: "Download the presentation in different formats : the format json allows importing your presentation while the zip format allows viewing your presentation offline"
    zip.notavailable: It's not yet possible to export as a Zip file
  preview:
    desc: View the presentation

search:
  empty: No results found for the search "%term%"

security:
  login:
    register: Register
    submit: Login
    forgot_password: Forgot password ?

profile:
  edit: Edit account details
  delete: Delete my account
  edit.cancel: Cancel edition

config:
  page_title: Configuration
  tab_menu:
    settings: Settings
    user_info: User informations
    password: Password
  form_settings:
    language_label: Language
  form_user:
    title: Modify your personal informations
    name_label: Username
    email_label: Email address
    delete:
      title: Delete your account
      description: If you don't use your account, you can easily delete it by clicking on the button below
      confirm: Confirm the deletion of the account
      button: Delete my account
  form_password:
    description: You want to change your password?
    old_password_label: Old password
    new_password_label: New password
    repeat_new_password_label: New password (confirm)
  form:
    save: Save
  reset:
    title: Reset your account
    description: Reset your account <b>will delete all your presentations</b> (including public models and presentations shared with your groups) as well as <b>all your pictures</b> and <b>all of the groups</b> of which you are the only administrator.
    confirm: Confirm the deletion of all your presentations



English: English
Français: French

version:
  title: Versions for the presentation %presentation%:
  lastnotdelete:
    desc: You can't delete the last version
    btn: Delete all old versions
  delete: Delete this version
  restore: Restore this version (creates a new version)
  download: Download this version
  purge: Delete all previous versions

errors.default: Error
info:
  default: Information
  version:
    restaured:
      title: Version restored
      desc: A new corresponding version has been created

template:
  form_settings:
    title_label: Presentation title
    template_label: Make this presentation a model
    public_label: Make this model public
    groups_label: Shares with a group
templates:
  title: Manage presentation %presentation%
  all: Templates
  form:
    maketemplate: Make this presentation a template
    maketemplate.tooltip: This presentation will be accessible as a base for other presentations inside « Your templates »
    makepublic: Make this template public
    makepublic.tooltip: This presentation will be accessible as a base for other presentations for everybody.
  fork:
    presentationtitle: Title for the new presentation
    presentationtitle.short: Title
    newpresentation: New presentation
    newpresentation.desc: You will create a new presentation based on this template
  mine: My templates (private)
  public: Public templates
  published: My published templates
  author: published by author
  empty: You have no templates
  publicempty: No one published templates yet

flashes:
  user:
    notice:
      added: User %username% added
      updated: User %username% updated
      deleted: User %username% deleted
  config:
    notice:
      config_saved: The configuration was saved
      presentation_reset: Your account was reset. All presentations, pictures and groups related to your account are deleted.
  group:
    notice:
      deleted: The group was deleted

preview:
  needlogin: You will need to login to edit this presentation

pictures:
  title: Pictures
  desc: This is the list of all the pictures that you've uploaded through your presentations. Note that the pictures associated to a presentation are deleted with it. Deleting pictures here will remove them from your presentations.
  message:
    deleted: The picture was deleted
  empty: You didn't send any pictures yet

# Admin stuff

user:
  manage:
    field:
      last_login: Last connection
      email: Email
      username: Username
    page_title: User management
    create_new_one: Create a new user
    edit_action: Edit the user
    action: Action
    description: Here you can manage your users (add, edit and delete)
  page_title: User management
  edit_user: Edit an exisiting user
  new_user: Create a new user
  form:
    email_label: Email
    username_label: Username
    enabled_label: Activated
    back_to_list: Back to the list
    delete_confirm: Confirm suppression?
    delete: Delete the user
    save: Save
    password_label: Password
    repeat_new_password_label: Repeat password

group:
  page_title: Groups
  list:
    name: Group name
    nbMembers: Number of members
    action: Action
    create_new_one: Create a new group
    empty: No groups found
  user:
    inGroup: "{0}There's no one in this group|{1}You are the only one in this group|]1,Inf[You are %count% users in this group"
    notInGroup: "{0}There's no one in this group|{1}There's only one user in this group|]1,Inf[%count% users in this group"
  presentations: Presentations shared with my groups
  leave: Leave the group
  join: Join the group
  form:
    name: Name of the group
    save: Create a new group
    role: Default rights for any new member
    access: Access method
    password: Password (if necessary)
  new.title: Creating a new group
  roles:
    readonly: Read-only
    write: Write
    manage_prez: Manage presentations
    manage_users: Manage members
    admin: Administrator
    unknown: Unknown
  access:
    open: Open
    request: Admission request
    password: Password
    invitation: On invite only
    hidden: On invite and hidden
    unknown: Unknown
  leave: Leave the group
  delete: Delete the group
  requests:
    page_title: Requests list
    username: Username
    action: Action
    sent: Request sent
  members:
    name: Member username
    role: Role
    action: Action
    edit: Edit member rights
    exclude: Exclude this member
    empty: You are the only member in this group
  manage:
    label: Manage the group
    title: Group « %group% » management
  edit_user:
    title: Editing member rights
    role: Member role
  edit:
    title: Edit the group
    requests: View pending requests
    invitations: View invitations
    cancel: Cancel
  menu:
    public: Public groups
    private: My groups
  password:
    title: Group protected with a password
    label: Enter the group's password
    submit: Join the group
  presentations: Presentations shared with this group
  invitation:
    list:
      name: Username
      date: Sent on
      title: Invitations
      empty: There's no pending invite for group « %group% »
    new:
      title: Send an invite
    email:
      hello: Hi %username%,
      invited: You have been invited to join the group « %groupname% » on Framaslides.
      join: You can join the group by clicking on this link <a href="%url%">%url%</a>.
      join_txt: You can join the group by clicking on this link %url%.
      cheers: Have a good day !
      subject: You have been invited to join %group% on Framaslides
introjs:
  nextLabel: Next
  prevLabel: Back
  skipLabel: Skip
  doneLabel: Done
  step1: Hi. This is where your presentations will be listed.
  step2: Create a new presentation here !
  step3: You can also click here.
  step4: If you need inspiration, have a look at the public templates
  step5: Why not create a group to share presentations between users ?
  step6: You can change view of your presentation list between list view and card view by clicking here.
