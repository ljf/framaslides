<?php

namespace Strut\UserBundle\Controller;

use Strut\StrutBundle\Entity\Config;
use Strut\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    /**
     * @Route("/locale", name="get-user-locale")
     * @return JsonResponse
     */
    public function getUserLocaleAction()
    {
        if ($this->getUser()) {
            $lang = $this->getUser()->getConfig()->getLanguage();
            return new JsonResponse(['lang' => $lang]);
        }
        return new JsonResponse([], 401);
    }

    /**
     * @Route("/firsttime", name="first-time")
     */
    public function setFirstTimeAction(): Response
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Config $config */
        $config = $this->get('strut.users_repository')->find($this->getUser()->getId())->getConfig();

        $config->setFirstTime(false);
        $em->persist($config);
        $em->flush();

        return new JsonResponse(['firsttime' => false]);
    }
}
