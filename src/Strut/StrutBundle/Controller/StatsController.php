<?php

namespace Strut\StrutBundle\Controller;

use DateInterval;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class StatsController extends Controller {

    /**
     * @Route("/stats", name="stats")
     *
     * @return Response
     */
    public function getStatisticsAction(): Response
    {
        $cache = new FilesystemAdapter();

        $stats = $cache->getItem('stats');
        $stats->expiresAfter(DateInterval::createFromDateString('3 hours'));
        if (!$stats->isHit()) {
            $stats->set($this->generateStats());
            $cache->save($stats);
        }
        return new JsonResponse($stats->get());
    }

    /**
     * @return array
     */
    private function generateStats(): array
    {
        $presentationRepository = $this->get('strut.presentation_repository');
        $nbPresentations = $presentationRepository->countPresentations();
        $nbPublicTemplates = $presentationRepository->countPublicTemplates();

        $groupRepository = $this->get('strut.group_repository');
        $nbGroups = $groupRepository->countGroups();

        $enabledUsers = $this->get('strut.users_repository')->getSumEnabledUsers();

        return [
            'presentations' => $nbPresentations,
            'public templates' => $nbPublicTemplates,
            'groups' => $nbGroups,
            'enabled users' => $enabledUsers,
            'version' => $this->getParameter('strut.version')
        ];
    }
}