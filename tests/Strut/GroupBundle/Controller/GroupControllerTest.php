<?php

namespace Tests\Strut\GroupBundle\Controller;

use Strut\GroupBundle\Entity\Group;
use Strut\UserBundle\Entity\User;
use Tests\Strut\StrutBundle\StrutTestCase;

class GroupControllerTest extends StrutTestCase
{
	public function testIndexPublicGroups()
	{
		$this->logInAs('admin');
		$client = $this->getClient();

		$client->request('GET', '/groups');

		$this->assertEquals(200, $client->getResponse()->getStatusCode(), 'Unexpected HTTP status code for GET /groups');

		$this->assertContains(htmlentities("bob's group", ENT_QUOTES), $client->getResponse()->getContent());
	}

	public function testNewGroup()
	{
		$this->logInAs('admin');

		$client = $this->getClient();

		$crawler = $client->request('GET', '/groups');

		$crawler = $client->click($crawler->filter('a.group-new')->link());

		$form = $crawler->filter('#group_save')->form([
			'group[name]' => 'group admin',
			'group[defaultRole]' => '1',
			'group[acceptSystem]' => '1',
			'group[plainPassword]' => '',
		]);

		$client->submit($form);
		$client->followRedirect();
		$crawler = $client->request('GET', '/groups/user-groups');

		$this->assertGreaterThan(0, $crawler->filter('td:contains("group admin")')->count(), 'Missing element group admin');
	}

	/**
	 * @depends testNewGroup
	 */
	public function testEditGroup()
	{
		$this->logInAs('admin');

		$client = $this->getClient();

		$crawler = $client->request('GET', '/groups/user-groups');

		$this->assertGreaterThan(0, $crawler->filter('td:contains("group admin")')->count(), 'Missing element group admin');

		$crawler = $client->click($crawler->filter('tr:contains("group admin") a.group-manage')->link());

		$crawler = $client->click($crawler->filter('a.group-edit')->link());

		$form = $crawler->filter('#group_save')->form([
			'group[name]' => 'group admin edited',
			'group[defaultRole]' => '1',
			'group[acceptSystem]' => '1',
			'group[plainPassword]' => '',
		]);

		$client->submit($form);
		$client->followRedirect();
		$crawler = $client->request('GET', '/groups/user-groups');

		$this->assertGreaterThan(0, $crawler->filter('td:contains("group admin edited")')->count(), 'Missing element group admin edited');
	}
}