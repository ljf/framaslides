<?php

namespace Tests\Wallabag\CoreBundle\Controller;

use Tests\Strut\StrutBundle\StrutTestCase;


class StatsControllerTest extends StrutTestCase
{
    public function testStats()
    {
        $client = $this->getClient();

        $client->request('GET', '/stats');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $content = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals($client->getContainer()->getParameter('strut.version'), $content['version']);

        /*
         * In order to test cache is active, let's delete an entry
         */
        $this->logInAs('admin');

        $contentId = $client->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('Strut\StrutBundle\Entity\Presentation')
            ->findOneByUser($this->getLoggedInUser())->getId();

        $client->request('GET', '/presentation/delete/id/' . $contentId);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $client->request('GET', '/stats');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertEquals($content, json_decode($client->getResponse()->getContent(), true));
    }
}